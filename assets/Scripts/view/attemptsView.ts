// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

// import Attempts from "../model/attempts";

const { ccclass, property } = cc._decorator;

@ccclass
export default class AttemptsView extends cc.Component {

    /** Отобразить попытки */
    showAttempts(attempts) {
        this.node.getComponent(cc.Label).string = attempts.toString();
    }
}
