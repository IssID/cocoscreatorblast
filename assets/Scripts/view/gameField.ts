// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameModel from "../model/gameModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class gameField extends cc.Component {

    gameModel: GameModel;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.gameModel = canvas.getComponent('gameModel');
    }

    start() {
        this.alignBackgroundGrid();
    }

    /** Выравниваем игровое поле по размерам сетки */
    alignBackgroundGrid() {
        this.node.scaleX = this.node.scaleX * this.gameModel.gridWidth;
        this.node.scaleY = this.node.scaleY * this.gameModel.gridHeight;
    }
}
