// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import blocksModel from "../model/blocksModel";
import mixModel from "../model/mixModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class mixBtn extends cc.Component {

    mixModel: mixModel;
    blocksModel: blocksModel;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.mixModel = canvas.getChildByName("models").getChildByName("mix").getComponent("mixModel");
        this.blocksModel = canvas.getChildByName("models").getChildByName("blocks").getComponent("blocksModel")

        this.node.on("touchstart", this.touchStart, this);
    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_END, this.touchStart, this);
    }

    touchStart() {
        if (!this.blocksModel.isBlocksMove()) {
            this.mixModel.mixGrid();
        }
    }
}
