// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import blocksModel from "../model/blocksModel";
import GameModel from "../model/gameModel";
import gridModel from "../model/gridModel";
import superBlockModel from "../model/superBlockModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class blockCheck extends cc.Component {

    /** Является ли супер блоком */
    superBlock = false;
    /** Первый активированый блок */
    firstBlock = false;
    /** Готов ли блок к удалению */
    isDelete = false;
    /** Находится ли в движении блок */
    isMoved = false;
    /** скорость анимации */
    speedBoost = 5;
    /** позиция блока для отслеживания в сетке */
    gridPosition;

    blocksModel: blocksModel;
    gameModel: GameModel;
    gridNode: cc.Node;
    gridModel: gridModel;
    superBlockModel: superBlockModel;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.gameModel = canvas.getComponent('gameModel');
        this.blocksModel = canvas.getChildByName("models").getChildByName("blocks").getComponent("blocksModel");
        this.superBlockModel = canvas.getChildByName("models").getChildByName("superBlock").getComponent("superBlockModel");
        this.gridNode = canvas.getChildByName("models").getChildByName("grid");
        this.gridModel = this.gridNode.getComponent('gridModel');

        this.node.on(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_END, this.onTouchEnd, this);
    }

    /** реакция на прикосновение к блоку */
    onTouchEnd(e: cc.Touch) {
        if (!this.blocksModel.isBlocksMove()) {
            if (this.superBlock) {
                this.superBlockModel.markBlocks(cc.v2(this.gridPosition.w, this.gridPosition.h));
            } else {
                this.firstBlock = true;
                this.checkMatch();
            }
        }
        this.gameModel.step();
    }

    /** Проверить блоки вокруг выбраного блока */
    checkMatch() {
        let worldPos = this.node.parent.convertToWorldSpaceAR(this.node.position);
        let selfBlock = cc.v2(worldPos.x, worldPos.y);

        // bottom
        let bottomBlock = cc.v2(worldPos.x, worldPos.y - this.node.height);
        this.checker(selfBlock, bottomBlock);
        // top
        let topBlock = cc.v2(worldPos.x, worldPos.y + this.node.height);
        this.checker(selfBlock, topBlock);
        // right
        let rightBlock = cc.v2(worldPos.x + this.node.width, worldPos.y);
        this.checker(selfBlock, rightBlock);
        // left
        let lefttBlock = cc.v2(worldPos.x - this.node.width, worldPos.y);
        this.checker(selfBlock, lefttBlock);
    }

    /** 
     * Отмечаем как для удаления если есть совпадения по цвету и передаюм в рекурсию 
     * @param selfBlock координаты первой точки cc.Vec2()
     * @param sideBlock координаты второй точки cc.Vec2()
     */
    checker(selfBlock: cc.Vec2, sideBlock: cc.Vec2) {
        let result = this.getRayResult(selfBlock, sideBlock);
        if (result.length) {
            let block = result.shift();
            if (this.node.name == block.collider.node.name) {
                let blockScript = block.collider.node.getComponent("blockAction");
                if (!blockScript.isDelete) {
                    blockScript.isDelete = true;
                    blockScript.checkMatch();
                }
            }
        }
    }

    /** 
     * Получить попадания луча 
     * @param p1 координаты первой точки cc.Vec2()
     * @param p2 координаты второй точки cc.Vec2()
     * @param raytype тип луча cc.RayCastType
     */
    getRayResult(p1: cc.Vec2, p2: cc.Vec2, raytype = cc.RayCastType.Closest): cc.PhysicsRayCastResult[] {
        return cc.director.getPhysicsManager().rayCast(p1, p2, raytype);
    }

    /** Опустить оставшиеся блоки после удаления */
    MoveDown(down: number) {
        let y = this.node.y - this.node.height * down;
        this.moveBlock(this.node.x, y, down / this.speedBoost);
    }

    /** 
     * Анимация блоков 
     * @param x координата
     * @param y координата
     * @param speed скорость анимации
     * @todo можно переделать для получения cc.Vec2, нужно ли?
     */
    private moveBlock(x: number, y: number, speed) {
        this.isMoved = true;
        cc.tween(this.node)
            .to(speed, { position: cc.v3(x, y, 0) })
            .call(() => { this.isMoved = false })
            .start();
    }
}
