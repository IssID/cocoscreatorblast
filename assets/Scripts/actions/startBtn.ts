// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

const { ccclass, property } = cc._decorator;

@ccclass
export default class StartBtn extends cc.Component {

    onLoad() {
        cc.director.preloadScene('level');

        this.node.on('touchstart', this.touchStart, this);
    }

    onDestroy() {
        this.node.off(cc.Node.EventType.TOUCH_END, this.touchStart, this);
    }

    touchStart() {
        cc.director.loadScene('level');
    }
}
