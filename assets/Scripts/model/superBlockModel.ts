// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameModel from "./gameModel";
import gridModel from "./gridModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class superBlockModel extends cc.Component {
    @property(cc.SpriteFrame)
    superSprite: cc.SpriteFrame = null;

    /** Нужное количество для создания суперблока */
    countRemoveBlockForSuper = 6;
    /** Радиус взрыва */
    radiusRemove = 2;

    gameModel: GameModel;
    gridModel: gridModel;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.gameModel = canvas.getComponent('gameModel');
        this.gridModel = canvas.getChildByName("models").getChildByName("grid").getComponent('gridModel');
    }

    /** Установить суперблок если это возможно */
    setSuperBlock(markedBlocks) {
        if (markedBlocks >= this.countRemoveBlockForSuper) {
            let firstBlock = this.getFirstBlock();
            if (firstBlock) {
                let blockAction = firstBlock.getComponent("blockAction");
                if (!blockAction.superBlock) {
                    firstBlock.name = 'super';
                    firstBlock.getComponent(cc.Sprite).spriteFrame = this.superSprite
                    blockAction.isDelete = false;
                    blockAction.superBlock = true;
                }
            }
        }
    }

    /** Получить первый нажатый блок перед удалением */
    getFirstBlock() {
        for (let w = 0; w < this.gameModel.gridWidth; w++) {
            for (let h = 0; h < this.gameModel.gridHeight; h++) {
                if (this.gridModel.grid[w][h].getComponent("blockAction").firstBlock) {
                    return this.gridModel.grid[w][h];
                }
            }
        }
    }

    /** Отметить блоки для уничтожения */
    markBlocks(pos) {
        for (let w = 0; w < this.gameModel.gridWidth; w++) {
            for (let h = 0; h < this.gameModel.gridHeight; h++) {
                if ((w - pos.x) * (w - pos.x) + (h - pos.y) * (h - pos.y) <= this.radiusRemove * this.radiusRemove) {
                    if (!this.gridModel.grid[w][h].getComponent("blockAction").isDelete) {
                        this.mark(w, h);
                    }
                }
            }
        }
    }

    /** Отметить конкретный блок для взрыва (если блок тоже супер активируем его) */
    mark(x, y) {
        if (x <= this.gameModel.gridWidth && x >= 0 && y <= this.gameModel.gridHeight && y >= 0) {
            this.gridModel.grid[x][y].getComponent("blockAction").isDelete = true;
            if (this.gridModel.grid[x][y].getComponent("blockAction").superBlock) {
                this.markBlocks(cc.v2(x, y));
            }
        }
    }
}
