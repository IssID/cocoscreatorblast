// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import AttemptsView from "../view/attemptsView";
import GameModel from "./gameModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Attempts extends cc.Component {

    /** Основная нода */
    canvas: cc.Node = null;

    /** Текущий счет */
    attempts: number;

    gameModel: GameModel;
    attemptsView: AttemptsView;

    onLoad() {
        this.canvas = cc.find("Canvas");
        this.attempts = this.canvas.getComponent('gameModel').attempts;
        this.attemptsView = this.canvas.getChildByName("panel_score").getChildByName("attempts").getComponent('attemptsView');
    }

    start() {
        this.attemptsView.showAttempts(this.attempts);
    }

    /** Уменьшаем попыткиж */
    gainAttempts() {
        this.attempts--;
        this.attemptsView.showAttempts(this.attempts);
    }
}
