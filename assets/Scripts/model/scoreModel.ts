// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import ScoreView from "../view/scoreView";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ScoreModel extends cc.Component {

    /** Текущий счет */
    score: number = 0;
    /** Требуемый счет */
    winScore: number;

    /** множитель счета */
    bonus = {
        needBlock: 5,
        increase: 2
    };

    scoreView: ScoreView;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.winScore = canvas.getComponent('gameModel').winScore;
        this.scoreView = canvas.getChildByName("panel_score").getChildByName("score").getComponent('scoreView');

        this.scoreView.showScore(this.winScore + "/" + this.score);
    }

    /** Увеличиваем счет */
    gainScore(mb: number) {
        if (mb >= this.bonus.needBlock) {
            this.score = this.score + mb * this.bonus.increase;
        } else {
            this.score = this.score + mb;
        }
        this.scoreView.showScore(this.winScore + "/" + this.score);
    }
}
