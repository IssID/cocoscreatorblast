// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import Attempts from "./attemptsModel";
import blocksModel from "./blocksModel";
import gridModel from "./gridModel";
import Score from "./scoreModel";
import superBlockModel from "./superBlockModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class GameModel extends cc.Component {
    /** Режим дебага */
    @property(cc.Boolean)
    debug: boolean = false;

    /** Минимальное количество клеток для удаления */
    @property(cc.Integer)
    blocksToDelete: number = 2;

    /** Количество цветов блока */
    @property(cc.Integer)
    colorCount: number = 5;

    /** Количество попыток */
    @property(cc.Integer)
    attempts: number = 10;

    /** Нужный счет для победы */
    @property(cc.Integer)
    winScore: number = 40;

    /** Количество попыток перемешать блоки*/
    @property(cc.Integer)
    mixCount: number = 1;

    /** Высота сетки */
    @property(cc.Integer)
    gridHeight: number = 9;

    /** Ширина сетки */
    @property(cc.Integer)
    gridWidth: number = 9;

    blocksModel: blocksModel;
    ScoreModel: Score;
    AttemptsModel: Attempts;
    gridModel: gridModel;
    superBlockModel: superBlockModel;
    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        cc.director.getPhysicsManager().enabled = true;
        if (this.debug) {
            cc.director.getPhysicsManager().debugDrawFlags = cc.PhysicsManager.DrawBits.e_jointBit
                | cc.PhysicsManager.DrawBits.e_aabbBit
                | cc.PhysicsManager.DrawBits.e_shapeBit
                | cc.PhysicsManager.DrawBits.e_jointBit
        }
        cc.director.preloadScene('lose');
        cc.director.preloadScene('win');

        this.blocksModel = this.node.getChildByName("models").getChildByName("blocks").getComponent("blocksModel");
        this.gridModel = this.node.getChildByName("models").getChildByName("grid").getComponent("gridModel");
        this.ScoreModel = this.node.getChildByName("models").getChildByName("score").getComponent('scoreModel');
        this.AttemptsModel = this.node.getChildByName("models").getChildByName("attempts").getComponent('attemptsModel');
        this.superBlockModel = this.node.getChildByName("models").getChildByName("superBlock").getComponent('superBlockModel');
    }

    /** Игрок сделал шаг */
    step() {
        let markedBlocks = this.blocksModel.getMarkedBlocks();
        this.superBlockModel.setSuperBlock(markedBlocks);
        this.blocksModel.removeBlocks(markedBlocks);
        this.blocksModel.checkAndMoveDownBlock();
        this.blocksModel.createNewBlocks();

        if (markedBlocks >= this.blocksToDelete) {
            this.ScoreModel.gainScore(markedBlocks);
            this.AttemptsModel.gainAttempts();
        }

        if (this.AttemptsModel.attempts <= 0)
            this.gameOver('lose');

        if (this.ScoreModel.score >= this.winScore && this.AttemptsModel.attempts > 0)
            this.gameOver('win');
    }

    /** Конец уровня, загружаем следующий */
    gameOver(lvlName: string) {
        cc.director.loadScene(lvlName);
    }
}
