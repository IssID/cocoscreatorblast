// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import blocksModel from "./blocksModel";
import GameModel from "./gameModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class gridModel extends cc.Component {

    /** многомерный массив для отслеживания блоков */
    grid;
    /** требуется для получения центра объекта или получения точного размера из за scale 0.5 */
    halfObject = 2;

    gameModel: GameModel;
    blocksModel: blocksModel;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.gameModel = canvas.getComponent('gameModel');
        this.blocksModel = canvas.getChildByName("models").getChildByName("blocks").getComponent("blocksModel");
    }

    start() {
        this.createGrid();
        this.createFillGrid();
    }

    /** 
     * Заполнить сетку блоками без анимации падения 
     * (Старт игры или для перемешивания блоков)
     */
    createFillGrid() {
        this.grid = [];
        for (var w = 0; w < this.gameModel.gridWidth; w++) {
            this.grid[w] = [];
            for (var h = 0; h < this.gameModel.gridHeight; h++) {
                let y = this.generatePositionY(h);
                let x = this.generatePositionX(w);
                this.blocksModel.createBlock(cc.v2(x, y));
                this.blocksModel.newBlock.getComponent("blockAction").gridPosition = { 'w': w, 'h': h }
                this.grid[w][h] = this.blocksModel.newBlock;
            }
        }
    }

    /** Получить позицию блока по его номеру строки (с правильным смещением) */
    generatePositionY(h: number, move = false): number {
        let blockHeight = this.blocksModel.blocks[0].data.height;
        // сдвиг каждого блока на его размер по вертикали
        let sideShiftY = blockHeight * h;
        // получить центр блока по горизонтали
        let centerOfBlockVertical = blockHeight / this.halfObject;
        // получаем центр сетки по горизонтали
        let centerOfGridVertical = this.node.height / this.halfObject;
        if (move) {
            return sideShiftY + centerOfBlockVertical + centerOfGridVertical
        }

        return sideShiftY + centerOfBlockVertical - centerOfGridVertical
    }

    /** Получить позицию блока по его номеру колонке (с правильным смещением) */
    generatePositionX(w: number): number {
        let blockWidth = this.blocksModel.blocks[0].data.width;
        // сдвиг каждого блока на его размер по горизонтали
        let sideShiftX = blockWidth * w;
        // получить центр блока по горизонтали
        let centerOfBlockHorizontally = blockWidth / this.halfObject;
        // получаем центр сетки по горизонтали
        let centerOfGridHorizontally = this.node.width / this.halfObject;

        return sideShiftX + centerOfBlockHorizontally - centerOfGridHorizontally
    }

    /** Создаем сетку по размерам заданым в gameModel */
    createGrid() {
        for (var w = 0; w < this.gameModel.gridWidth; w++) {
            this.node.width = this.node.width + this.blocksModel.blocks[0].data.width;
        }
        for (var h = 0; h < this.gameModel.gridHeight; h++) {
            this.node.height = this.node.height + this.blocksModel.blocks[0].data.height;
        }
    }

    /** перемешать блоки (удалить и создать новую сетку блоков) */
    mixBlock() {
        for (let w = 0; w < this.gameModel.gridWidth; w++) {
            for (let h = 0; h < this.gameModel.gridHeight; h++) {
                this.grid[w][h].getComponent("blockAction").isDelete = true;
            }
        }

        this.blocksModel.removeBlocks(this.gameModel.gridWidth * this.gameModel.gridHeight);
        this.createFillGrid();
    }
}
