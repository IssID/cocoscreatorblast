// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import mixView from "../view/mixView";
import gridModel from "./gridModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class mixModel extends cc.Component {

    /** Отображение счета */
    mixCount: number;

    gridModel: gridModel;
    mixView: mixView;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.mixCount = canvas.getComponent('gameModel').mixCount
        this.gridModel = canvas.getChildByName("models").getChildByName("grid").getComponent('gridModel');
        this.mixView = canvas.getChildByName("bonus_btn").getChildByName("mix").getComponent('mixView');
        
        this.mixView.showMixCount(this.mixCount);
    }

    /** Замешать сетку */
    mixGrid() {
        if (this.mixCount > 0) {
            this.gridModel.mixBlock();
            this.mixCount--;
            this.mixView.showMixCount(this.mixCount);
        }
    }
}
