// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameModel from "./gameModel";
import gridModel from "./gridModel";

const { ccclass, property } = cc._decorator;

@ccclass
export default class blocksModel extends cc.Component {
    @property(cc.Prefab)
    blueBlock: cc.Prefab = null;

    @property(cc.Prefab)
    greenBlock: cc.Prefab = null;

    @property(cc.Prefab)
    purpleBlock: cc.Prefab = null;

    @property(cc.Prefab)
    redBlock: cc.Prefab = null;

    @property(cc.Prefab)
    yellowBlock: cc.Prefab = null;

    /** Префабы блоков */
    blocks: object;

    newBlock;

    gameModel: GameModel;
    gridModel: gridModel;

    onLoad() {
        let canvas = cc.find("Canvas");
        this.gameModel = canvas.getComponent('gameModel');
        this.gridModel = canvas.getChildByName("models").getChildByName("grid").getComponent('gridModel');

        this.blocks = {
            0: this.blueBlock,
            1: this.greenBlock,
            2: this.purpleBlock,
            3: this.redBlock,
            4: this.yellowBlock,
        };
    }

    /** Генератор случайных целых чисел */
    private getRandomInt(max: number): number {
        return Math.floor(Math.random() * max);
    }

    /** вернуть случанйного номер цвета */
    private getRandomColorNum(): number {
        return this.getRandomInt(this.gameModel.colorCount);
    }

    // создать блок по координатам
    createBlock(pos: cc.Vec2) {
        let blockPrefab = this.blocks[this.getRandomColorNum()];
        this.newBlock = cc.instantiate(blockPrefab);
        this.newBlock.position = pos;
        this.node.addChild(this.newBlock);
    }

    /** Создаем недостающие блоки с анимацией падения */
    createNewBlocks() {
        for (var w = 0; w < this.gameModel.gridWidth; w++) {
            for (var h = 0; h < this.gameModel.gridHeight; h++) {
                if (!this.gridModel.grid[w][h]) {
                    let y = this.gridModel.generatePositionY(h, true)
                    let x = this.gridModel.generatePositionX(w);
                    this.createBlock(cc.v2(x, y));
                    this.newBlock.getComponent("blockAction").gridPosition = { 'w': w, 'h': h }
                    this.gridModel.grid[w][h] = this.newBlock;
                    this.newBlock.getComponent("blockAction").MoveDown(this.gameModel.gridHeight);
                }
            }
        }
    }

    /** Проверяем какие блоки нужно уронить и роняем */
    checkAndMoveDownBlock() {
        for (let w = 0; w < this.gameModel.gridWidth; w++) {
            /** количество блоков на которые нужо опуститься */
            let down = 0;
            for (let h = 0; h < this.gameModel.gridHeight; h++) {
                if (!this.gridModel.grid[w][h]) {
                    down++;
                } else {
                    if (down > 0) {
                        this.gridModel.grid[w][h].getComponent("blockAction").MoveDown(down);
                        this.gridModel.grid[w][h].getComponent("blockAction").gridPosition.h = h - down;
                        this.gridModel.grid[w][h - down] = this.gridModel.grid[w][h];
                        this.gridModel.grid[w][h] = null;
                    }
                }
            }
        }
    }

    /** Удаляем блоки если таковые есть */
    removeBlocks(markedBlocks: number) {
        for (let w = 0; w < this.gameModel.gridWidth; w++) {
            for (let h = 0; h < this.gameModel.gridHeight; h++) {
                this.gridModel.grid[w][h].getComponent("blockAction").firstBlock = false;
                if (markedBlocks >= this.gameModel.blocksToDelete
                    && this.gridModel.grid[w][h].getComponent("blockAction").isDelete
                ) {
                    this.gridModel.grid[w][h].destroy();
                    this.gridModel.grid[w][h] = null;
                } else {
                    this.gridModel.grid[w][h].getComponent("blockAction").isDelete = false;
                }
            }
        }
    }

    /** Получаем помеченые блоки на удаление */
    getMarkedBlocks(): number {
        let markedBlocks = 0;
        for (var w = 0; w < this.gameModel.gridWidth; w++) {
            for (var h = 0; h < this.gameModel.gridHeight; h++) {
                if (this.gridModel.grid[w][h].getComponent("blockAction").isDelete) {
                    markedBlocks++;
                }
            }
        }
        return markedBlocks;
    }

    /** Проверяем в движении ли блоки */
    isBlocksMove(): boolean {
        for (var w = 0; w < this.gameModel.gridWidth; w++) {
            for (var h = 0; h < this.gameModel.gridHeight; h++) {
                if (this.gridModel.grid[w][h].getComponent("blockAction").isMoved) {
                    return true;
                }
            }
        }
        return false;
    }

    /** Проверяет есть ли еще ходы или больше нет вариантов */
    checkGrid(): boolean {
        for (let w = 0; w < this.gameModel.gridWidth; w++) {
            for (let h = 0; h < this.gameModel.gridHeight; h++) {
                this.gridModel.grid[w][h].getComponent("blockAction").checkMatch();
            }
        }

        let checked = false;
        for (let w = 0; w < this.gameModel.gridWidth; w++) {
            for (let h = 0; h < this.gameModel.gridHeight; h++) {
                if (this.gridModel.grid[w][h].getComponent("blockAction").isDelete) {
                    checked = true;
                }
                this.gridModel.grid[w][h].getComponent("blockAction").isDelete = false;
            }
        }
        return checked;
    }
}
